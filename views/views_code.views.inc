<?php

/**
 * @file
 * Views hook implementations and related functions.
 *
 * @author Hein Bekker <hein@netbek.co.za>
 * @copyright (c) 2015 Hein Bekker
 * @license http://www.gnu.org/licenses/gpl-2.0.txt GPLv2
 */

/**
 * Returns a list of machine names of Views implemented by this module
 *
 * @return array
 */
function views_code_get_module_views() {
  $values = variable_get('views_code_views', '');

  if ($values === '') {
    return array();
  }

  $arr = explode("\n", $values);
  $parsed = array();

  foreach ($arr as $value) {
    $parts = explode(':', $value);
    list($view_name, $module_name) = $parts;

    $parsed[$value] = array(
      'view_name' => $view_name,
      'module_name' => $module_name,
    );
  }

  return $parsed;
}

/**
 * Deletes a View implemented by this module
 *
 * @param array $view
 * @return boolean
 */
function views_code_delete_module_view($view) {
  $loaded_view = views_get_view($view['view_name']);

  if (empty($loaded_view)) {
    drupal_set_message(t('View <code>!name</code> not found.', array('!name' => $view['view_name'])), 'warning');
    return FALSE;
  }

  views_delete_view($loaded_view);
  return TRUE;
}

/**
 * Deletes all Views implemented by this module
 */
function views_code_delete_module_views() {
  foreach (views_code_get_module_views() as $view) {
    views_code_delete_module_view($view);
  }
  return TRUE;
}

/**
 * Returns exportable code for Views implemented by this module
 *
 * @param boolean $write_to_file Set to TRUE to write to code to file. This will
 * overwrite the existing file. Requires write permission.
 * @return boolean|array
 */
function views_code_export_module_views($write_to_file = FALSE) {
  $outputs = array();

  foreach (views_code_get_module_views() as $view) {
    if (empty($view['module_name']) || !module_exists($view['module_name'])) {
      drupal_set_message(t('Module <code>!name</code> not found.', array('!name' => $view['module_name'])), 'warning');
    }
    else {
      if (!array_key_exists($view['module_name'], $outputs)) {
        $outputs[$view['module_name']] = '';
      }

      $loaded_view = views_get_view($view['view_name']);
      if (empty($loaded_view)) {
        drupal_set_message(t('View <code>!name</code> not found.', array('!name' => $view['view_name'])), 'warning');
      }
      else {
        $outputs[$view['module_name']] .= trim(views_export_view($loaded_view)) . "\n" . '$views[$view->name] = $view;' . "\n\n";
      }
    }
  }

  if ($write_to_file) {
    foreach ($outputs as $module_name => $output) {
      $output = '<?php

/**
 * @file
 * Default views definitions.
 */

/**
 * Implements hook_views_default_views().
 */
function ' . $module_name . '_views_default_views() {
  $views = array();
' . $output . '  return $views;
}
';

      $file_path = views_code_export_module_views_path($module_name);

      if (!file_exists($file_path) || is_writable($file_path)) {
        if (file_put_contents($file_path, $output) === FALSE) {
          drupal_set_message(t('Failed to write to <code>!file</code>. Please check the write permissions.', array('!file' => pathinfo($file_path, PATHINFO_BASENAME))), 'error');
          return FALSE;
        }
      }
      else {
        drupal_set_message(t('<code>!file</code> not writable. Please check the write permissions.', array('!file' => pathinfo($file_path, PATHINFO_BASENAME))), 'error');
        return FALSE;
      }
    }

    views_flush_caches();
    return TRUE;
  }

  return $outputs;
}

/**
 * Check if Views file of a module is writable
 *
 * @param string $module_name
 * @return boolean
 */
function views_code_export_module_views_path($module_name) {
  $file_name = $module_name . '.views_default.inc';
  return drupal_get_path('module', $module_name) . '/views/' . $file_name;
}

/**
 * Check if Views files of all modules are writable
 *
 * @return boolean|array
 */
function views_code_export_module_views_writable() {
  $views = views_code_get_module_views();

  if (empty($views)) {
    return TRUE;
  }

  $errors = array();

  foreach ($views as $view) {
    $file_path = views_code_export_module_views_path($view['module_name']);

    if (!(!file_exists($file_path) || is_writable($file_path))) {
      $errors[] = pathinfo($file_path, PATHINFO_BASENAME);
    }
  }

  if (empty($errors)) {
    return TRUE;
  }

  return array_unique($errors);
}
