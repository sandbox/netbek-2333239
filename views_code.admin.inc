<?php

/**
 * @file
 * Admin page callbacks for the Views Code module.
 *
 * @author Hein Bekker <hein@netbek.co.za>
 * @copyright (c) 2015 Hein Bekker
 * @license http://www.gnu.org/licenses/gpl-2.0.txt GPLv2
 */

/**
 * Settings form builder.
 */
function views_code_admin_settings() {
  $form = array();

  $views = variable_get('views_code_views', '');
  $form['views_code_views'] = array(
    '#type' => 'textarea',
    '#title' => t('Machine name of views to manage and export destinations (modules)'),
    '#description' => t('Each value should be on a new line. Format of value: view_name:module_name'),
    '#default_value' => $views,
  );

  return system_settings_form($form);
}

/**
 * Validation callback for the settings form.
 */
function views_code_admin_settings_validate($form, &$form_state) {
  $views = $form_state['values']['views_code_views'];

  if (!empty($views)) {
    $views = str_replace(array("\r\n", "\r"), "\n", trim($views));
    $arr = explode("\n", $views);
    foreach ($arr as $key => $value) {
      $value = trim($value);
      if ($value === '' || strpos($value, ':') === FALSE) {
        unset($arr[$key]);
      }
      else {
        $arr[$key] = trim($value);
      }
    }
    $arr = array_unique($arr);
    $form_state['values']['views_code_views'] = implode("\n", $arr);
  }
}

/**
 * Form builder for export form.
 */
function views_code_admin_export_form($form, &$form_state) {
  if (($writable = views_code_export_module_views_writable()) === TRUE) {
    $description = t('Views in code, views in database and views overriding code will be exported.');
  }
  else {
    $description = t('<b>Please grant write permission on <code>!file</code> before exporting.</b>', array('!file' => implode(', ', $writable)));
  }

  $form['export_views'] = array(
    '#type' => 'fieldset',
    '#title' => t('Export managed views to code'),
    '#description' => $description,
  );
  $form['export_views']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Export views'),
    '#submit' => array('views_code_admin_views_export'),
  );

  return $form;
}

/**
 * Form builder for delete form.
 */
function views_code_admin_delete_form($form, &$form_state) {
  $form['delete_views'] = array(
    '#type' => 'fieldset',
    '#title' => t('Delete managed views'),
    '#description' => t('Views in database and views overriding code will be deleted. <b>Views in code will remain intact.</b>'),
  );
  $form['delete_views']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete views'),
    '#submit' => array('views_code_admin_views_delete'),
  );

  return $form;
}

/* -----------------------------------------------------------------------------
 * VIEWS
 * -------------------------------------------------------------------------- */

/**
 * Form submit handler for export form. Exports views.
 */
function views_code_admin_views_export() {
  if (views_code_export_module_views(TRUE)) {
    $count = count(views_code_get_module_views());
    drupal_set_message(format_plural($count, '@count view processed.', '@count views processed.'));
    drupal_set_message(t('If you wish to delete views in the database that are overriding code, i.e. to load all views from code, then run <code>delete views</code> now.'));
  }
  else {
    drupal_set_message(t('The views could not be exported to code.'));
  }
}

/**
 * Form submit handler for delete form. Deletes views.
 */
function views_code_admin_views_delete() {
  $operations = array();

  foreach (views_code_get_module_views() as $view) {
    $operations[] = array('views_code_admin_views_delete_batch', array($view));
  }

  if (empty($operations)) {
    drupal_set_message('There are no views to delete.');
  }
  else {
    $batch = array(
      'title' => 'Deleting views',
      'operations' => $operations,
      'finished' => 'views_code_admin_views_delete_batch_finished',
      'file' => drupal_get_path('module', 'views_code') . '/views_code.admin.inc',
    );
    batch_set($batch);
  }
}

/**
 * Batch operation.
 *
 * @param array $view
 * @param array $context
 */
function views_code_admin_views_delete_batch($view, &$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = 1;
  }

  views_code_delete_module_view($view);

  $context['results'][] = $view;
  $context['sandbox']['progress'] ++;

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Batch finished callback.
 */
function views_code_admin_views_delete_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = format_plural(count($results), '@count view processed.', '@count views processed.');
  }
  else {
    $error_operation = reset($operations);
    $message = 'An error occurred while deleting ' . $error_operation[0] . ' with arguments :' . print_r($error_operation[0], TRUE);
  }

  drupal_set_message($message);
}
